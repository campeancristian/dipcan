#include "dc_main.h"
#include "main.h"
#include "dc_usb.h"
#include "dc_can.h"
#include "usb_device.h"
#include "dc_wifi.h"
#include "dc_controller.h"
#include "dc_packet_processor.h"
#include "dc_buzzer.h"
#include "dc_infocollector_module.h"
//---------------------------------------------------------------------------------------------------------------------
TaskHandle_t dc_wifi_txTaskHandle;
//---------------------------------------------------------------------------------------------------------------------
void dc_main_commander (dc_usb_packet_t * packet){
	if (packet->data[0] == T_MAIN_CTRL && packet->len >= 2){
		if (packet->data[1]){
			NVIC_SystemReset();
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void dc_initTask( void *pvParameters ){
	// peripheral init functions
	dc_usb_init();
	//dc_wifi_init();
	//dc_controller_init();
	//dc_packet_processor_init();
	dc_buzzer_init();


	vTaskDelay(pdMS_TO_TICKS(500));

	// creating tasks
	xTaskCreate( dc_usb_tx_task,		"usbTxTask", 		configMINIMAL_STACK_SIZE, NULL, 1, NULL );
	xTaskCreate( dc_usb_rx_task,		"usbRxTask", 		256, NULL, 3, NULL );

	xTaskCreate( dc_can_rx_task,		"canRxTask", 		configMINIMAL_STACK_SIZE, NULL, 1, NULL );
	xTaskCreate( dc_can_tx_task, 		"canTxTask", 		configMINIMAL_STACK_SIZE, NULL, 1, NULL );

	xTaskCreate( dc_infocollector_task, "collectorTask", 	512, NULL, 1, NULL );

	//xTaskCreate( dc_wifi_txTask,	"wifiTx", 256, NULL, 1, &dc_wifi_txTaskHandle);
	//xTaskCreate( dc_wifi_rxTask,	"wifiRx", 256, NULL, 3, &dc_wifi_txTaskHandle);

	//xTaskCreate( dc_controller_task, "control", 256, NULL, 3, NULL);

	dc_can_init();

	// hello world
	dc_usb_send_str("I am alive!\r\n");

	//dc_initial_dashboard_test();

	vTaskDelete(NULL);
}

