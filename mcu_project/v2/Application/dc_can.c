#include "dc_can.h"
#include <string.h>
#include "main.h"
#include "dc_main.h"
#include "dc_forwarder_module.h"
#include "dc_infocollector_module.h"
//---------------------------------------------------------------------------------------------------------------------
extern CAN_HandleTypeDef hcan1;
extern CAN_HandleTypeDef hcan2;
extern CAN_HandleTypeDef hcan3;

CAN_HandleTypeDef * canHandles[] = { &hcan1, &hcan2, &hcan3 };
//---------------------------------------------------------------------------------------------------------------------
static QueueHandle_t txBufferQueue;
static QueueHandle_t rxBufferQueue;
//---------------------------------------------------------------------------------------------------------------------
typedef struct{
	bool 		isActive;
	TickType_t 	lastPacket;
} dc_can_status_t;

dc_can_status_t canStatus[CAN_MAX_CH];
//---------------------------------------------------------------------------------------------------------------------
void dc_ls_can_init(void){
	if (HAL_CAN_Start(&hcan3) == HAL_OK){
		//if (HAL_CAN_ActivateNotification(&hcan3, CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK) { Error_Handler(); }
		if (HAL_CAN_ActivateNotification(&hcan3, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK) { Error_Handler(); }
		if (HAL_CAN_ActivateNotification(&hcan3, CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK) { Error_Handler(); }
		if (HAL_CAN_ActivateNotification(&hcan3, CAN_IT_ERROR) != HAL_OK) { Error_Handler(); }

		canStatus[CAN_L].lastPacket = 0;
		canStatus[CAN_L].isActive = true;
		dc_usb_send_str("LSCAN inited\r\n");
	} else {
		dc_usb_send_str("LSCAN init failed\r\n");
	}
}
//---------------------------------------------------------------------------------------------------------------------
void dc_ls_can_deinit(void){
	HAL_CAN_Stop(&hcan3);
	HAL_CAN_DeInit(&hcan3);
	canStatus[CAN_L].isActive = false;
}
//---------------------------------------------------------------------------------------------------------------------
void dc_can_init(void){
	if (HAL_CAN_Start(&hcan1) != HAL_OK) { Error_Handler();}
	//if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_ERROR) != HAL_OK) { Error_Handler(); }


	if (HAL_CAN_Start(&hcan2) != HAL_OK) { Error_Handler(); }
	//if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_ERROR) != HAL_OK) { Error_Handler(); }

	dc_ls_can_init();

	canStatus[CAN_M].lastPacket = 0;
	canStatus[CAN_M].isActive = true;
	canStatus[CAN_H].lastPacket = 0;
	canStatus[CAN_H].isActive = true;

	HAL_GPIO_WritePin(HS_CAN_RS_GPIO_Port, HS_CAN_RS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MS_CAN_RS_GPIO_Port, MS_CAN_RS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LS_CAN_RS_GPIO_Port, LS_CAN_RS_Pin, GPIO_PIN_RESET);

	txBufferQueue = xQueueCreate( DC_CAN_TX_BUFFER_SIZE, sizeof(dc_can_packet_t));
	rxBufferQueue = xQueueCreate( DC_CAN_RX_BUFFER_SIZE, sizeof(dc_can_packet_t));
}
//---------------------------------------------------------------------------------------------------------------------
// Sending
//---------------------------------------------------------------------------------------------------------------------
void dc_can_send_packet(dc_can_ch_t canChannel, uint32_t id, uint8_t dlc, uint8_t * pData){
	dc_can_packet_t 	txPacket = {
			.canCh 		= canChannel,
			.id 		= id,
			.dlc		= dlc > DC_CAN_MAX_DATA_LEN ? DC_CAN_MAX_DATA_LEN : dlc,
	};
	memcpy(txPacket.data, pData, txPacket.dlc);

	if (is_handler_mode()){
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		if ( xQueueSendFromISR(txBufferQueue, &txPacket, &xHigherPriorityTaskWoken) != pdTRUE ){
			Error_Handler();
		}
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	} else {
		if ( xQueueSend(txBufferQueue, &txPacket, 10) != pdTRUE ){
			Error_Handler();
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void dc_can_tx_task(void * pvParameters){
	uint32_t           	txMailbox;
	dc_can_packet_t 		txPacket;
	CAN_TxHeaderTypeDef	txHeader = {
			.ExtId = 0,
			.RTR = CAN_RTR_DATA,
			.IDE = CAN_ID_STD,
			.TransmitGlobalTime = DISABLE
	};

	for(;;){
		if ( xQueueReceive(txBufferQueue, &txPacket, portMAX_DELAY) == pdTRUE ){
			txHeader.StdId = txPacket.id;
			txHeader.DLC = txPacket.dlc;
			if (!canStatus[txPacket.canCh].isActive){ continue; }

			if (HAL_CAN_AddTxMessage(canHandles[txPacket.canCh], &txHeader, txPacket.data, &txMailbox) == HAL_OK){
				//dc_usb_send_str("sending...\r\n");
			} else {
				Error_Handler();
				// packet could be put at the end of the queue for resending
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
// Receiving
//---------------------------------------------------------------------------------------------------------------------
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){
	CAN_RxHeaderTypeDef rxHeader;
	dc_can_packet_t		rxPacket;

	HAL_CAN_DeactivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING);

	if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &rxHeader, rxPacket.data) == HAL_OK ) {
		BaseType_t		xHigherPriorityTaskWoken = pdFALSE;
		rxPacket.dlc 	= rxHeader.DLC;
		rxPacket.id 	= rxHeader.StdId;
		rxPacket.canCh	= hcan == canHandles[CAN_H] ? CAN_H : hcan == canHandles[CAN_M] ? CAN_M : CAN_L;

		if (rxHeader.RTR != CAN_RTR_DATA || rxHeader.IDE != CAN_ID_STD){
			Error_Handler();	// TODO delete this after making sure it never happens!
		}

		if ( xQueueSendFromISR(rxBufferQueue, &rxPacket, &xHigherPriorityTaskWoken) != pdTRUE ){
			Error_Handler();
		}
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}

	HAL_CAN_ActivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING );
}
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
void dc_can_rx_task( void * pvParameters){
	dc_can_packet_t		rxPacket;

	for(;;){
		if ( xQueueReceive(rxBufferQueue, &rxPacket, portMAX_DELAY) == pdTRUE ){
			// TODO after 1500ms of inactivity on LSCAN, it could be de-inited?

			// Sharing the incoming packet with the modules
			dc_forwarder_can_rx(&rxPacket);
			dc_infocollector_can_rx(&rxPacket);
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan){ Error_Handler(); }
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan){ Error_Handler(); } // TODO delete this after making sure it never happens!
void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan){}
void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan){}
void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan){}



