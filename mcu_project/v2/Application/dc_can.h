#ifndef DC_CAN_H_
#define DC_CAN_H_
//--------------------------------------------------------------------------------------------------------------------
#include "stm32f4xx.h"
//---------------------------------------------------------------------------------------------------------------------
extern CAN_HandleTypeDef  * canHandles[];
//---------------------------------------------------------------------------------------------------------------------
#define DC_CAN_TX_BUFFER_SIZE	50
#define DC_CAN_RX_BUFFER_SIZE	50

#define DC_CAN_MAX_DATA_LEN		8
//---------------------------------------------------------------------------------------------------------------------
typedef enum{
	CAN_H = 0,
	CAN_M,
	CAN_L,
	CAN_MAX_CH,
} dc_can_ch_t;

typedef struct{
	dc_can_ch_t canCh;
	uint32_t 	id;
	uint8_t		dlc;
	//uint8_t		rtr;
	//uint8_t		ide;
	uint8_t		data[DC_CAN_MAX_DATA_LEN];
} dc_can_packet_t;
//---------------------------------------------------------------------------------------------------------------------
void dc_can_init(void);
void dc_can_rx_task(void * pvParameters);
void dc_can_tx_task(void * pvParameters);
void dc_can_rx_handler(CAN_HandleTypeDef *hcan);
void dc_can_send_packet(dc_can_ch_t canChannel, uint32_t id, uint8_t dlc, uint8_t * pData);
//---------------------------------------------------------------------------------------------------------------------
#endif /* DC_CAN_H_ */
