#include "dc_forwarder_module.h"
#include "dc_main.h"
#include "dc_usb.h"
#include "dc_can_defines.h"
#include <stdlib.h>
//--------------------------------------------------------------------------------------------------------------------
static bool 		canRxForwardList[CAN_MAX_CH];
static dc_can_ch_t 	canTxForwardCh = CAN_H;

//--------------------------------------------------------------------------------------------------------------------
void dc_forwarder_init(void){
	canRxForwardList[CAN_L] = false;
	canRxForwardList[CAN_M] = false;
	canRxForwardList[CAN_H] = false;
}
//--------------------------------------------------------------------------------------------------------------------
void dc_forwarder_usb_rx(dc_usb_packet_t * usbPacket){
	if (usbPacket->data[0] == T_FORWARDER_RX_SET && usbPacket->len >= 2){
		canRxForwardList[CAN_L] = usbPacket->data[1] & 0x01 ? true : false;
		canRxForwardList[CAN_M] = usbPacket->data[1] & 0x02 ? true : false;
		canRxForwardList[CAN_H] = usbPacket->data[1] & 0x04 ? true : false;
		char str[200];
		sprintf(str, "- forwarder rx L/M/H: %d %d %d\r\n", canRxForwardList[CAN_L], canRxForwardList[CAN_M], canRxForwardList[CAN_H]);
		dc_usb_send_str(str);
	}

	if (usbPacket->data[0] == T_FORWARDER_TX_SET && usbPacket->len >= 2){
		switch (usbPacket->data[1]){
			case 0: canTxForwardCh = CAN_L; break;
			case 1: canTxForwardCh = CAN_M; break;
			case 2: canTxForwardCh = CAN_H; break;
			default: break;
		}
		dc_usb_send_str("- forwarder tx setter\r\n");
	}

	if (usbPacket->data[0] == T_FORWARDER_SEND && usbPacket->len >= 9){
		dc_can_packet_t canPacket = {
				.dlc = 0,
		};
		memset(canPacket.data, 0, 8);
		char * ptr;
		canPacket.id = (uint32_t)strtol((char*)&(usbPacket->data[1]), &ptr, 16);
		if ((uint8_t*)ptr != &usbPacket->data[5]) { return; } //sanity check on the first semicolon
		uint8_t data, i = 6; // incoming packet format: @IDXX,data....\n
		while (i < usbPacket->len && usbPacket->data[i] != '\n'){
			data = usbPacket->data[i] >= '0' && usbPacket->data[i] <= '9' ? usbPacket->data[i] - '0' : 0;
			data = usbPacket->data[i] >= 'a' && usbPacket->data[i] <= 'f' ? usbPacket->data[i] - 'a' + 10 : data;
			canPacket.data[canPacket.dlc] |= i%2 ? data & 0x0F : data << 4;
			i++;
			canPacket.dlc = (uint8_t)((i-6)/2);
		}
		if (canPacket.dlc > 8) { return; }
		dc_can_send_packet(canTxForwardCh, canPacket.id, canPacket.dlc, canPacket.data);
	}
}
//--------------------------------------------------------------------------------------------------------------------
void dc_forwarder_can_rx(dc_can_packet_t * canPacket){
	if (canRxForwardList[canPacket->canCh]){
		dc_usb_packet_t usbPacket;

		sprintf((char*)usbPacket.data, "%X,", (unsigned int)canPacket->id);
		for (uint8_t i = 0; i < canPacket->dlc; i++){
			sprintf((char*)usbPacket.data + strlen((char*)usbPacket.data), "%02X", canPacket->data[i]);
		}
		sprintf((char*)usbPacket.data + strlen((char*)usbPacket.data), "\n");
		usbPacket.len = strlen((char*)usbPacket.data);
		dc_usb_send_data(&usbPacket);
	}
}
//--------------------------------------------------------------------------------------------------------------------

