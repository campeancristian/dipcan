#ifndef DC_MAIN_H_
#define DC_MAIN_H_
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "printf-stdarg.h"
#include <stdbool.h>
#include <string.h>
#include "dc_usb.h"
//---------------------------------------------------------------------------------------------------------------------
void dc_initTask( void *pvParameters );
void dc_main_commander (dc_usb_packet_t * packet);
//---------------------------------------------------------------------------------------------------------------------

#endif /* DC_MAIN_H_ */
